# Forum

A simple student forum made for the college assignment.

## Installation

```bash
# install dependencies
composer install

# create database

# edit .env
rename .env.example to .env and set right credentials
(DB_DATABASE, DB_USERNAME, DB_PASSWORD)

# set application key for encryption
php artisan key:generate

# fill database
php artisan migrate --seed

# serve file
php artisan serve
```

## Usage

You can log in and create threads, join discussions, comment on other student's threads etc.





## License
[MIT](https://choosealicense.com/licenses/mit/)